import React, { useState } from 'react';
import Map from '../partials/Map';
import emailjs, { send } from 'emailjs-com';
import ReCAPTCHA from "react-google-recaptcha";
import InputField from '../atoms/InputField';
import TextInput from '../atoms/TextInput';
import DataGerman from '../../assets/german.json';
import DataEnglish from '../../assets/english.json';





  function Contact() {

    const [emailSent, setEmailSent] = useState(false);
    const [status, setStatus] = useState(0);
    const [recaptch, setRecaptch] = useState(0);
    const [recaptchError, setRecaptchError] = useState(false);
    const [validation, setValidation] = useState([
      {id: 'firstname', valid: false},
      {id: 'lastname', valid: false},
      {id: 'email', valid: false},
      {id: 'subject', valid: false},
      {id: 'message', valid: false},
    ]);

    let data;

    if(process.env.REACT_APP_ENV === "german") {
      data = DataGerman;
    } else {
      data = DataEnglish;
    }


    function sendEmail(e) {
      e.preventDefault();
      if(recaptch.length > 0){
        setRecaptchError(false);
        emailjs.sendForm('service_c9o0gq9', 'template_24hf0yq', e.target, 'user_GYb86TljsvXzEPbLzKP57')
        .then((result) => {
            setEmailSent(true);
            setStatus(result.status);
        }, (error) => {
            console.log(error.text);
        });
      }else{
        console.log("error");
        setRecaptchError(true);
      }
    }
  
    
    function onChange(value) {
      setRecaptch(value);
    }

    const validate = (data) => {
      let exists;
      let item;
      let index;
      if(validation.find(object => object.id === data.id)){
          exists = true;
          item = validation.find(item => item.id === data.id);
          index = validation.findIndex(x => x.id === data.id);
          if(!exists){
            setValidation(oldArray => [...oldArray,data] );
            console.log('item does not exist yet');
          }
      };
      if(exists){
        if(data.valid === item.valid){
        }else{
          index = validation.findIndex(x => x.id === data.id);
          let newArr = [...validation]; // copying the old datas array
          newArr[index] = data;
          setValidation(newArr);
          if(validation !== 'undefined'){
            validation[index].valid = true;
          }
        }
      }else{
        setValidation(oldArray => [...oldArray,data] );
      }
  }

const checkValid = () => {
  let i;
  let x = 0;
  for(i = 0; i<validation.length; i++){
    if(!validation[i].valid){
      return false;
    }else{
      return true;
    }
  }
}

      

  return <div class="row page contact">
    <div className="col-xl-6 col-12 contact__col">
        <h1 class="contact__col__h1 contrast-h1 sec-color">
            {data.contact.headline}
        </h1>
        <form class="contact__col__form" onSubmit={checkValid ? sendEmail : console.log("not valid")}>
             <div class="row contact__col__form__names">
                <div class="col-md-6">
                <InputField giveClass="contact__col__form__wrap" 
                label={data.contact.labels[0]}
                htmlFor="firstname" 
                type="text"
                htmlID="firstname"
                htmlName={data.contact.labels[0]}
                emailSent={emailSent}
                onchange={(e) => { validate(e) }}
                />
                </div>
                <div class="col-md-6">
                <InputField giveClass="contact__col__form__wrap" 
                label={data.contact.labels[1]}
                htmlFor="lastname" 
                type="text"
                htmlID="lastname"
                htmlName={data.contact.labels[1]}
                emailSent={emailSent}
                onchange={(e) => { validate(e) }}
                />
                </div>
            </div>
            <div class="row contact__col__form__subject">
                <div class="col-md-6">
                    <InputField giveClass="contact__col__form__wrap" 
                    label={data.contact.labels[2]}
                    htmlFor={data.contact.labels[2]}
                    type="text"
                    htmlID="subject"
                    htmlName={data.contact.labels[2]}
                    emailSent={emailSent}
                    onchange={(e) => { validate(e) }}
                    />
                </div>
                <div class="col-md-6">
                    <InputField giveClass="contact__col__form__wrap" 
                    label="Email" 
                    htmlFor="email" 
                    type="text"
                    htmlID="email"
                    htmlName="Email"
                    emailSent={emailSent}
                    onchange={(e) => { validate(e) }}
                    />
                </div>
            </div>
            <div class="row contact__col__form__message">
              <div class="col-12">
              <TextInput giveClass="contact__col__form__wrap" 
                    label={data.contact.labels[4]}
                    htmlFor="message" 
                    type="text"
                    htmlID="message"
                    htmlName={data.contact.labels[4]}
                    emailSent={emailSent}
                    onchange={(e) => { validate(e) }}
                    />
              </div>
                  
            </div>
            <div className="row contact__col__form__button">
                <div className="col-12 col-md-6">
                
                  <div className="button">
                  <input type="submit" value="Let's create together!" />
                  </div>
                </div>
                
                <div className="col-12 col-md-6">
                  <p className="text-s sec-color">{status === 200 ? 'Vielen Dank für Ihre Anfrage! Ihre Nachricht wird so schnell wie möglich beantwortet' : ''}</p>
                </div> 
                
            </div>
            <div className={recaptchError ? 'recaptcha row recaptcha--error' : 'recaptcha row'}>
              <div className="col-md-6 col-12">
              <ReCAPTCHA
                  sitekey="6LfnSHYcAAAAANE-pjGlWlDRW1RVDochNPG4vvUE"
                  onChange={onChange}
                />
              </div>
            </div>
            
        </form>
    </div>
    <div className="col-xl-6 col-12 contact__col contact__col--map">
        <Map></Map>
    </div>
  
</div>;
}
 
export default Contact;
