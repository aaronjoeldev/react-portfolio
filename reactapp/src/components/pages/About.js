import React from 'react';
import { Wave } from 'react-animated-text';
import Profilbild from '../../assets/img/Berwerbungsfoto.png'
import DataGerman from '../../assets/german.json';
import DataEnglish from '../../assets/english.json';



  function About() {
  let data;

  if(process.env.REACT_APP_ENV === "german") {
    data = DataGerman;
  } else {
    data = DataEnglish;
  }
 
  return <div class="row page about">
  
  <div class="col-lg-6 col-md-6 col-12 about__col">
  <img src={Profilbild} alt="Profilbild" />
      <h1 class="about__col__h1 h3 sec-color text-uppercase">
          <span class="about__col__h1__wrapper">
              <span class="about__col__h1__letters"> 
              <Wave text="Aaron Joel" 
                  speed = "8"
                  iterations = "1"
              />

     
            </span>
              <span class="about__col__h1__letters--noanime">Kaemmerer-Karsten</span>
          </span>
      </h1>


  </div>
  <div class="col-lg-6 col-md-6 col-12 about__col">
      <h2 class="about__col__h2 contrast-h1 sec-color">{data.about.headline}</h2>
      <p class="about__col__text sec-color text-m">
        {data.about.text}
      </p>
  </div>
</div>;
}
 
export default About;
