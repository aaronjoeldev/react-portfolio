import React, {useState } from 'react';
import { Link,  } from 'react-router-dom';
import DataEnglish from '../../assets/english.json';
import DataGerman from '../../assets/german.json';





  function Navigation() {
    const [open, setOpen] = useState(false);
    let data;
    if(process.env.REACT_APP_ENV === "german") {
      data = DataGerman;
    } else {
      data = DataEnglish;
    }
  return <div className="navigation"> 
  <div className={ open ? 'overlay-navigation overlay-active' : 'overlay-navigation'}>
  <nav role="navigation">
    <ul>
      <li className={ open ? 'slide-in-nav-item' : 'slide-in-nav-item-reverse'} onClick={() => setTimeout(function(){setOpen(false)}, 1000)}>
      <Link to="/about" data-content="The beginning" class="contrast-h2 sec-color nav-item" 
      data-page="about">{data.navigation[0]}</Link></li>
      <li className={ open ? 'slide-in-nav-item-delay-1' : 'slide-in-nav-item-delay-1-reverse'} onClick={() => setTimeout(function(){setOpen(false)}, 1000)}>
      <Link to="/vita" data-content="Mehr erfahren" class="contrast-h2 sec-color nav-item" data-page="vita">{data.navigation[1]}</Link></li>
      <li className={ open ? 'slide-in-nav-item-delay-2' : 'slide-in-nav-item-delay-2-reverse'} onClick={() => setTimeout(function(){setOpen(false)}, 1000)}>
      <Link to="/projects" data-content="Mehr erfahren" class="contrast-h2 sec-color nav-item" data-page="projects">{data.navigation[2]}</Link></li>
      <li className={ open ? 'slide-in-nav-item-delay-3' : 'slide-in-nav-item-delay-3-reverse'} onClick={() => setTimeout(function(){setOpen(false)}, 1000)}>
      <Link to="/skills" data-content="Mehr erfahren" class="contrast-h2 sec-color nav-item" data-page="skills">{data.navigation[3]}</Link></li>
      <li className={ open ? 'slide-in-nav-item-delay-4' : 'slide-in-nav-item-delay-4-reverse'} onClick={() => setTimeout(function(){setOpen(false)}, 1000)}>
      <Link to="/contact" data-content="Mehr erfahren" class="contrast-h2 sec-color nav-item" data-page="skills">{data.navigation[4]}</Link>
      {process.env.REACT_APP_ENV === "german" &&
      <div className="legals">
            <Link to="/imprint">{data.navigation[5]}</Link>
            <Link to="/dataprotection">{data.navigation[6]}</Link>
        </div>
      }
        
      </li>
      
    </ul>
  </nav>
</div>
<div className="open-overlay" onMouseDown={() => open ? setOpen(false) : setOpen(true)}>
  <span className={ open ? 'bar-top animate-top-bar' : 'bar-top animate-out-top-bar'}></span>
  <span className={ open ? 'bar-middle animate-middle-bar' : 'bar-top animate-out-middle-bar'}></span>
  <span className={ open ? 'bar-bottom animate-bottom-bar' : 'bar-bottom animate-out-bottom-bar'}></span>
</div>
</div>;
}
 
export default Navigation;
