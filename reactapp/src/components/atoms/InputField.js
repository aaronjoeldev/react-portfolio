import React, { useState, useEffect } from 'react';


const InputField = (props) => {
    const [value, setValue] = useState('');
    const [valid, setValid] = useState(false);
    const handleChange = (e) => {
        setValue(e.target.value);
    }
    const emptyVal = () => {
        if(props.emailSent){
            setValue('');
        }else{
            return value;
        }
        return value;
    }
    useEffect(() => {
        if(value.length){
            setValid(true);
        }else{
            setValid(false);
        }
        props.onchange({id: props.htmlID, valid: valid}); 
        if(props.emailSent){
            setValue('');
        }else{
            return value;
        }
    }, [handleChange]);
    return (
      <div className={props.giveClass}>
        <input type={props.type} value={value} id={props.htmlID} name={props.htmlName} onChange={handleChange} required />
        <label className={value && 'filled'} for={props.htmlFor} >
          {props.label}
        </label>
        <span className="form__active"></span>
      </div>
    );
  }
 
export default InputField;
