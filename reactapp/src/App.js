import {useState, useEffect} from 'react';
import { useLocation } from 'react-router-dom';
import Load from './components/partials/Load';
import Footer from './components/partials/Footer';
import Navigation from './components/partials/Navigation';
import Routes from './components/partials/Routes';
import Back from './components/partials/Back';





function App() {
 const [change, setChange] = useState(true);
const location = useLocation();
const [currentPath, setCurrentPath] = useState(location);



 useEffect(() => {
   const  pathname  = location.pathname;
   setChange(true);
   setTimeout(function(){
      setChange(false);
   },3000)
   setCurrentPath(pathname);
 }, [location.pathname]);

  return (
    <div className="App container-fluid">
    <Navigation></Navigation>
    
    {location.pathname.length > 3 ?
      <Back></Back>: <div class="d-none"></div>
    }
    <Load change={change}></Load>
    <Routes></Routes>
    {location.pathname.length > 3 ?
      <Footer></Footer> : <div class="d-none"></div>
    }
    
    
    
    </div>
  );
}

export default App;
