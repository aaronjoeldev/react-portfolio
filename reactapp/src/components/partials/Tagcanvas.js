import React, { useEffect} from 'react';



const Tagcanvas = (props) => {


  

    useEffect(() => {
      if(props.color === "white"){
        eval(
          `try {
             TagCanvas.Start(
               'skills', 'tags',
               {textColour: '#ededed',
               outlineColour: 'transparent',
               reverse: true,
               depth: 1,
               weightMode:'both',
               weight: true,
               textHeight: 40,
               dragControl: true,
               initial: [0.130, 0.080],
               maxSpeed: 0.06,}
             );
             if(window.innerWidth < 1000){
               TagCanvas.Options({textHeight: 20});
             }
           }
           catch(e) {
            
           }`
         );
      } else{
        eval(
          `try {
             
             if(window.innerWidth < 1000){
              TagCanvas.Start(
                'skills', 'tags',
                {textColour: '#F30939',
                outlineColour: 'transparent',
                reverse: true,
                depth: 1,
                weightMode:'both',
                weight: true,
                textHeight: 20,
                dragControl: true,
                initial: [0.130, 0.080],
                maxSpeed: 0.06,}
              );
             }else{
              TagCanvas.Start(
                'skills', 'tags',
                {textColour: '#F30939',
                outlineColour: 'transparent',
                reverse: true,
                depth: 1,
                weightMode:'both',
                weight: true,
                textHeight: 40,
                dragControl: true,
                initial: [0.130, 0.080],
                maxSpeed: 0.06,}
              );
             }
             
           }
           catch(e) {
             
           }`
         );
      }
      }, [props.color]);



  return <div className="tagcanvas">
      <div class="skills__col__container" id="skillsContainer">
              <canvas width={ window.innerWidth < 1000 ? window.width : 800} height={ window.innerWidth < 1000 ? window.innerWidth / 1.5 : 439} id="skills" class="skills__col__container__canvas">
              </canvas>
            </div>
            <div id="tags" class="skills__col__container__tags sec-color">
              <a href="javascript:void(0);">HTML</a>
              <a href="javascript:void(0);">CSS</a>
              <a href="javascript:void(0);">SCSS</a>
              <a href="javascript:void(0);">BEM</a>
              <a href="javascript:void(0);">Bootstrap</a>
              <a href="javascript:void(0);">Javascript</a>
              <a href="javascript:void(0);">jQuery</a>
              <a href="javascript:void(0);">Photoshop</a>
              <a href="javascript:void(0);">InDesign</a>
              <a href="javascript:void(0);">Illustrator</a>
              <a href="javascript:void(0);">XD</a>
              <a href="javascript:void(0);">Git</a>
              <a href="javascript:void(0);">ReactJS</a>
              <a href="javascript:void(0);">VueJS</a>
              <a href="javascript:void(0);">NuxtJS</a>
              <a href="javascript:void(0);">TypeScript</a>
              <a href="javascript:void(0);">GSAP</a>
              <a href="javascript:void(0);">Headless CMS</a>
              <a href="javascript:void(0);">Strapi</a>
             
            </div>
            </div>;
}
 
export default Tagcanvas;


