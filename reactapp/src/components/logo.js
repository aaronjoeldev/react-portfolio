
import React, {useState} from 'react';
import Anime, { anime } from 'react-anime';


function Logo() {
  
  return <div className="row load">
    <Anime
    initial={[
        {
        targets: "#load",
        translateX: 50,
        easing: "linear"
        }
    ]}
    ></Anime>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 994.79 569.61" id="load">
            <defs>
            </defs>
            <g id="Ebene_2" data-name="Ebene 2" class="lines">
                <polygon class="cls-1 lines" points="212.01 287.56 397.97 566.51 285.75 566.51 189.56 566.51 3.6 287.56 189.56 5.41 295.37 162.52 212.01 287.56"/>
                <polygon class="cls-1 lines" points="782.73 287.56 601.13 565.97 708.98 566.51 799.13 549.97 991.13 287.56 805.17 5.41 699.36 162.52 782.73 287.56"/>
                <polygon class="cls-2 lines" points="351.6 264.58 413.53 375.74 313.17 557.08 307.92 566.6 189.56 566.51 186.78 562.34 199.58 539.2 351.6 264.58"/>
                <polygon class="cls-2 lines" points="721.13 565.97 597.44 566.6 592.16 557.08 499.77 390.06 454.48 308.28 452.68 305.02 390.74 193.84 390.95 193.48 395.9 184.53 436.19 111.75 440.93 101.51 464.58 101.51 469.15 111.72 509.46 184.53 705.76 539.2 721.13 565.97"/>
                <path class="cls-2 lines" d="M667,474c40.43,0,95.73-17.23,121.34-48.31V107.83h98.43V456.08l-2.66,4.4c-16.86,27.91-41.07,69.45-67.88,88-39,27-94.61,23.48-142.23,23.48" transform="translate(-2.87 -6.03)"/>
                <path d="M393,199.84" transform="translate(-2.87 -6.03)"/>
            </g>
        </svg>  
    </div>;
}
 
export default Logo;



/**
$( document ).ready(function() {
    $('.load').hide();
    $('.start__col__back__button, .nav-item').on('click', function(){
      $('.load').show();
      let logo = anime({
        targets: '.lines .lines',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1500,
        delay: function(el, i) { return i * 250 },
        direction: 'alternate',
        loop: true
      });
      setTimeout(function(){
        $('.load').hide();
      },2500)
    });
    
  });
   
 */