import React from 'react';
import Graph from "react-graph-vis";



function Diagram() {
    const white = '#ededed';
    const red = '#ff073a';
    const graph = {
        nodes: [
            {
                id: 211,
                label: '2013',
                color: {
                    background: red,
                    border: white
                },
                font:{size: 30, color: red, face: 'Pathway Gothic One'},
                group: 1
              },
              {
                id: 212,
                label: '2016',
                color: {
                    background: red,
                    border: white
                },
                font:{size: 30, color: red, face: 'Pathway Gothic One'},
                group: 1
              },
              {
                id: 213,
                label: '2020',
                color: {
                    background: red,
                    border: white
                },
                font:{size: 30, color: red, face: 'Pathway Gothic One'},
                group: 1
              },
              {
                id: 14,
                label: '2021',
                color: {
                    background: red,
                    border: white
                },
                group: 1,
                font:{size: 30, color: red, face: 'Pathway Gothic One'}
              },
              {
                id: 16,
                label: '<b>Abitur</b>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2,
              },
              {
                id: 17,
                label: '<i>Maximilian-Kolbe<Gymnasium</i>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 18,
                label: '<b>Medieninformatik</b>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 19,
                label: '<i>Technische Hochschule Köln</i>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 110,
                label: '<b>Mediengestalter</b>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 111,
                label: '<i>Ausbildung abgeschlossen</i>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 112,
                label: '<b>Frontend-Entwickler</b>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 113,
                label: '<i>Angestellt</i>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2,
              },
        ],
        edges: [
            {from: 11, to: 12},
            {from: 11, to: 16},
            {from: 11, to: 17},
            {from: 12, to: 13},
            {from: 12, to: 18},
            {from: 12, to: 19},
            {from: 13, to: 14},
            {from: 13, to: 110},
            {from: 13, to: 111},
            {from: 14, to: 112},
            {from: 14, to: 113},
        ]
      };
    
      const options = 
      {
        nodes: {
            size: 10,
            shape: 'dot',
            color: red,
        font: {
          size: 12,
          multi: true,
          strokeWidth: 0,
          color: white
        },
        
        scaling: {
          min: 10,
          max: 100,
        },
      },
      physics: {
        stabilization: {
          iterations: 500     // YMMV; higher value takes longer but results in better layout
        },
        solver: 'barnesHut',
		barnesHut: {
			gravitationalConstant: -24300,
			springConstant: 0.1,
            springLength: 30,
			avoidOverlap: 1,
		},
        stabilization: { iterations: 2500 },
      },
        groups: {
            1: {color:{background: red}, font:{size: 30, color: red, face: 'Pathway Gothic One'}, size: 10},
            2: {color:{background: white}, font:{size: 5, face: 'Archivo'},size: 10}
          }, 
        edges: {
            color: {color: white},
            smooth: false,
            width: 3
        }
      };
    

      var moveToOptions = {
        position: {x:300, y:300},    // position to animate to (Numbers)
        scale: 3.0,              // scale to animate to  (Number)
        //offset: {x:300, y:300},      // offset from the center in DOM pixels (Numbers)
        animation: {             // animation object, can also be Boolean
        duration: 1000,                 // animation duration in milliseconds (Number)
        easingFunction: "easeInOutQuad" // Animation easing function, available are:
        }                                   // linear, easeInQuad, easeOutQuad, easeInOutQuad,
    } 



      const events = {
          
        select: 
        ({ pointer: { canvas, nodes } }) => {
            console.log(canvas);
            moveToOptions.position.x = canvas.x;
            moveToOptions.position.y = canvas.y;
          }
      };




  return <div className="diagramm">
     <Graph
      graph={graph}
      options={options}
      events={events}
      scale={100}
      getNetwork={network => {
        console.log(network);
          network.on('click', function(properties) {
            network.moveTo(moveToOptions);
    });
          
        //  if you want access to vis.js network api you can set the state in a parent component using this property
      }}
    />
  </div>;
}
 
export default Diagram;


