// gulp.js einbauen
var gulp = require('gulp'); 

config = require("./gulp-config.json");

// Plugins einbauen
sass = require('gulp-sass'),
concatCss = require('gulp-concat-css'),
csso = require('gulp-csso'),
uglify = require('gulp-uglify'),
concat = require('gulp-concat'), 
runSequence = require('gulp4-run-sequence');


gulp.task('sass', function () {   
  return gulp.src(config.scss.src)  
         .pipe(sass())
          .pipe(concatCss('styles.css'))
          .pipe(gulp.dest(config.css.destDev)); 
});




gulp.task('minify', function minifyFunc() {
  return gulp.src(config.css.destDev)
  .pipe(csso({
    restructure: false,
    sourceMap: true,
    debug: true
  }))
    .pipe(gulp.dest(config.css.destBuild));
});



gulp.task('js', function(){    
    return gulp.src(['src/js/modules/*.js'])          
        .pipe(concat('all.js'))       
        .pipe(uglify())       
        .pipe(gulp.dest(jsOut)); 
});

gulp.task('jsLibs', function(){    
  return gulp.src(['src/js/libs/jquery.js', 'src/js/libs/*.js'])  
      .pipe(concat('libs.js'))             
      .pipe(gulp.dest(jsOut)); 
});


gulp.task('fileinclude', function() {
   return gulp.src(['src/templates/*.html'])
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(gulp.dest('www/'));
  });





gulp.task('watch', function() {
  gulp.watch(config.scss.src, gulp.series('sass')); 
  //gulp.watch('src/js/modules/*.js', gulp.series('js')); 
  //gulp.watch('src/js/libs/*.js', gulp.series('jsLibs')); 
  //gulp.watch('src/partials/*.html', gulp.series('fileinclude')); 
  //gulp.watch('src/templates/*.html', gulp.series('fileinclude')); 
  //gulp.watch("src/templates/*.html").on('change', browserSync.reload);
});



 gulp.task('build', function (callback) {
  runSequence(
    'sass',
    'minify',
    'js',
    'fileinclude',
    callback
//  ^^^^^^^^
//  This informs that the sequence is complete.
  );
});