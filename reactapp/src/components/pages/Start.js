import React, {useState, useEffect} from 'react';
import {
  Link
} from "react-router-dom";
import Tagcanvas from '../partials/Tagcanvas';
import Slides from '../partials/Carousel';
import Next from '../partials/Next';
import VitaImg from '../../assets/img/vita.png'
import Arrow from '../../assets/img/arrow-down.svg'
import DataGerman from '../../assets/german.json';
import DataEnglish from '../../assets/english.json';







function Start() {
  
  const [hData, setHData] = useState(0);
  const [width, setWidth] = useState();
  const innerWidth = window.innerWidth;
  let data;

  if(process.env.REACT_APP_ENV === "german") {
    data = DataGerman;
  } else {
    data = DataEnglish;
  }

  useEffect(() => {
    setWidth(window.innerWidth);
    window.addEventListener("resize", () => setWidth(window.innerWidth));
  }, []);

  


  
  return <div className="row start">
  <h1 className="start__h1 h1 sec-color" id="startH">
      <span className="start__h1__left">
          <span className={ hData === "1" && width > 700 ? 'h1JS hoverJS_col h1ani' : 'h1JS hoverJS_col'} 
          onMouseOver={() => setHData("1")}
          >Fron </span>
          <span className={ hData === "2" && width > 700 ? 'h1JS hoverJS_col h1ani' : 'h1JS hoverJS_col'}
          onMouseOver={() => setHData("2")}
          >tend</span>
      </span>
      <span className="start__h1__right">
      <span className={ hData === "3" && width > 700 ? 'h1JS hoverJS_col h1ani' : 'h1JS hoverJS_col'}
          onMouseOver={() => setHData("3")}>{data.start[4].headline[0]}</span>
          <span className={ hData === "4" && width > 700 ? 'h1JS hoverJS_col h1ani' : 'h1JS hoverJS_col'}
          onMouseOver={() => setHData("4")}>{data.start[4].headline[1]}</span>
      </span>
  </h1>
  <div className={ hData === "1" ? 'col-lg-3 col-md-3 start__col hoverJS_col start__col--hover ' : 'col-lg-3 col-md-3 start__col hoverJS_col '} data-place="1" 
  onMouseOver={() => setHData("1")}
   >
      <div className="start__col__front"> 
          <h3 className={ width > 700 ? "contrast-h1 sec-color" : "contrast-h2 sec-color"}>{data.start[0]}</h3>
      </div>
      <div className="start__col__back">
          <p className={innerWidth > 700 ? "start__col__back__text text-m sec-color" : "start__col__back__text text-s sec-color" }>
              <span className="d-block"><span class="highlight">A</span>aron <span class="highlight">J</span>oel <span>Kaemmerer-Karsten</span></span>
              <span className="d-block">{data.start[4].info}</span>
              <span className="d-block">Denmark</span>
          </p>
          {innerWidth > 700 ? 
          
            <button className="start__col__back__button text-m" data-page="about">
                <Link to="/about" className="sec-color">
                <svg viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg"><title/><path d="M81.8457,25.3876a6.0239,6.0239,0,0,0-8.45.7676L48,56.6257l-25.396-30.47a5.999,5.999,0,1,0-9.2114,7.6879L43.3943,69.8452a5.9969,5.9969,0,0,0,9.2114,0L82.6074,33.8431A6.0076,6.0076,0,0,0,81.8457,25.3876Z"/></svg>
                </Link>
          </button>
          :
          <Next page="about"></Next> 
          }
          
      </div>
  </div>
  <div className={ hData === "2" ? 'col-lg-3 col-md-3 start__col hoverJS_col start__col--hover ' : 'col-lg-3 col-md-3 start__col hoverJS_col '} data-place="2" 
  onMouseOver={() => setHData("2")}>
      <div className="start__col__front"> 
          <h3 className={ width > 700 ? "contrast-h1 sec-color" : "contrast-h2 sec-color"}>{data.start[1]}</h3>
      </div>
      <div className="start__col__back">
      <img src={VitaImg} alt="Vita Preview" width="100" height="80" className="vita-prev"/>
      {innerWidth > 700 ? 
            <button className="start__col__back__button text-m" data-page="vita">
                <Link to="/vita" className="sec-color"><svg viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg"><title/><path d="M81.8457,25.3876a6.0239,6.0239,0,0,0-8.45.7676L48,56.6257l-25.396-30.47a5.999,5.999,0,1,0-9.2114,7.6879L43.3943,69.8452a5.9969,5.9969,0,0,0,9.2114,0L82.6074,33.8431A6.0076,6.0076,0,0,0,81.8457,25.3876Z"/></svg></Link>
          </button>
          :
          <Next page="vita"></Next>
          }
      </div>
  </div>
  <div className={ hData === "3" ? 'col-lg-3 col-md-3 start__col hoverJS_col start__col--hover ' : 'col-lg-3 col-md-3 start__col hoverJS_col '} data-place="3" 
  onMouseOver={() => setHData("3")}>
      <div className="start__col__front"> 
          <h3 className={ width > 700 ? "contrast-h1 sec-color" : "contrast-h2 sec-color"}>{data.start[2]}</h3>
      </div>
      <div className="start__col__back">
        <Slides></Slides>
        {innerWidth > 700 ? 
            <button className="start__col__back__button  text-m" data-page="projects">
                <Link to="/projects" className="sec-color"><svg viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg"><title/><path d="M81.8457,25.3876a6.0239,6.0239,0,0,0-8.45.7676L48,56.6257l-25.396-30.47a5.999,5.999,0,1,0-9.2114,7.6879L43.3943,69.8452a5.9969,5.9969,0,0,0,9.2114,0L82.6074,33.8431A6.0076,6.0076,0,0,0,81.8457,25.3876Z"/></svg></Link>
          </button>
          :
          <Next page="projects"></Next>
          }
      </div>
  </div> 
  <div  className={ hData === "4" ? 'col-lg-3 col-md-3 start__col hoverJS_col start__col--hover ' : 'col-lg-3 col-md-3 start__col hoverJS_col '} data-place="4" onMouseOver={() => setHData("4")}>
      <div className="start__col__front"> 
          <h3 className={ width > 700 ? "contrast-h1 sec-color" : "contrast-h2 sec-color"}>{data.start[3]}</h3>
      </div>
      <div className="start__col__back">
          <Tagcanvas color="white"></Tagcanvas>
          {innerWidth > 700 ? 
            <button className="start__col__back__button  text-m" data-page="skills">
                <Link to="/skills" className="sec-color"><svg viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg"><title/><path d="M81.8457,25.3876a6.0239,6.0239,0,0,0-8.45.7676L48,56.6257l-25.396-30.47a5.999,5.999,0,1,0-9.2114,7.6879L43.3943,69.8452a5.9969,5.9969,0,0,0,9.2114,0L82.6074,33.8431A6.0076,6.0076,0,0,0,81.8457,25.3876Z"/></svg></Link>
          </button>
          :
          <Next page="skills"></Next>
          }
          
          
      </div>
  </div>
</div>;
}
 
export default Start;

