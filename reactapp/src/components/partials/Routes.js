
import { Route, Switch, withRouter } from 'react-router-dom';
import Start from '../pages/Start';
import Load from './Load';
import Skills from '../pages/Skills';
import Vita from '../pages/Vita';
import About from '../pages/About';
import Imprint from '../legals/Imprint'; 
import Projects from '../pages/Projects'; 
import Contact from '../pages/Contact'; 
import DataProtection from '../legals/DataProtection'; 



function Routes() {
  

  return (
  <Switch>
    <Route path="/" component={Start} exact/>
    <Route path="/skills" component={withRouter(Skills)} />
    <Route path="/vita" component={withRouter(Vita)} />
    <Route path="/about" component={withRouter(About)} />
    <Route path="/projects" component={withRouter(Projects)} />
    <Route path="/load" component={withRouter(Load)}/>
    <Route path="/imprint" component={withRouter(Imprint)}/>
    <Route path="/dataprotection" component={withRouter(DataProtection)}/>
    <Route path="/contact" component={withRouter(Contact)}/>
  </Switch>
);
}
 
export default Routes;

