import React from 'react';
import Ref1 from '../../assets/img/react-memory.png'
import Ref2 from '../../assets/img/corona-react.png'
import Ref3 from '../../assets/img/react-recipe.png'
import DataGerman from '../../assets/german.json';
import DataEnglish from '../../assets/english.json';

  function Projects() {
  let data;

    if(process.env.REACT_APP_ENV === "german") {
      data = DataGerman;
    } else {
      data = DataEnglish;
    }

  return <div class="row page projects">
  
  
  <div class="col-lg-12 col-12 projects__col">
    <h1 class="skills__col__h1 contrast-h1 sec-color">{data.projects.headline}</h1>
      <div class="row">
          <div class="col-12 project">
            <a href="https://aaronjoeldev.github.io/memory/" target="_blank" class="more">
                <img src={Ref1} alt="React memory" />
              </a>
              <div class="projects__col__desc">
                  <p class="projects__col__desc--headline sec-color h3">React Memory</p>
                  <p class="projects__col__desc--desc sec-color text-m">{data.projects.texts[0]}</p>
              </div>
          </div>

          <div class="col-12 project project--reverse">
              <a href="https://aaronjoeldev.github.io/covid-19/" target="_blank" class="more">
                <img src={Ref2} alt="React corona" />
              </a>
              <div class="projects__col__desc">
                  <p class="projects__col__desc--headline sec-color h3">React Corona</p>
                  <p class="projects__col__desc--desc sec-color text-m">{data.projects.texts[1]}</p>
              </div>
          </div>

         <div class="col-12 project">
            <a href="http://aaronjoel.de/react/recipe-app/index" target="_blank" class="more">
                <img src={Ref3} alt="React recipe" />
              </a>
              <div class="projects__col__desc">
                  <p class="projects__col__desc--headline sec-color h3">React Vegan Recipes</p>
                  <p class="projects__col__desc--desc sec-color text-m">{data.projects.texts[2]}</p>
              </div>
          </div>
      </div>
  </div>
</div>;
}
 
export default Projects;
