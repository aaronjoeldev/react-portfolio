import React, { useState, useEffect } from 'react';


const TextInput = (props) => {
    const [value, setValue] = useState('');
    const [valid, setValid] = useState(false);
    const handleChange = (e) => {
      setValue(e.target.value);
      props.onchange({id: props.htmlID, valid: valid});
    }

    useEffect(() => {
        if(value.length){
            setValid(true);
        }
        if(props.emailSent){
          setValue('');
        }else{
            return value;
        }
        
    }, [handleChange]);
    return (
      <div className={props.giveClass}>
      <textarea type={props.type} value={value} id={props.htmlID} name={props.htmlName} onChange={handleChange} required />
      <label className={value && 'filled'} for={props.htmlFor} >
        {props.label}
      </label>
      <span className="form__active"></span>
    </div>
    );
  }
 
export default TextInput;
