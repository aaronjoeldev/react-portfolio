import React, {useState} from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../logo.svg'
import LogoI from '../../logo_inverted.svg'


const LogoHome = (props) =>  {

    const[hover, setHover] = useState(false);


  return <div className="logo home" onMouseOver={() => setHover(true)} onMouseLeave={() => setHover(false)}>
        <Link to="/">
            <img src={hover ? Logo : LogoI}/>
        </Link>
    </div>;
}
 
export default LogoHome;

