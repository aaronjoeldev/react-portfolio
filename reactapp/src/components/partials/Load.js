
import React, {useState, useEffect} from 'react';
import DrawSVG from 'react-svg-drawing'



const Logo = (props) =>  {

  const [loop, setLoop] = useState(false);


useEffect(() => {
  if(props.change){
    setLoop(true);
  }else{
    setLoop(false);
  }
  
}, [props.change]);


  return <div className={ props.change ? "row load blur" : "row load d-none"}>
  
      {props.change ?
        <DrawSVG
      targets = ".lines"
      easing = "easeInOutExpo"
      duration = "1500"
      >
        
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 994.79 569.61" id="load">
          <g id="Ebene_2" data-name="Ebene 2" className="lines">
            <g id="Ebene_2-2" data-name="Ebene 2">
              <polyline className="cls-1 lines"points="350.13 493.97 397.97 566.51 285.75 566.51 189.56 566.51 3.6 287.56 189.56 5.41 295.37 162.52 212.01 287.56 280.57 390.41"/>
              <polyline className="cls-1 lines" points="867.79 100.41 805.17 5.41 699.36 162.52 782.73 287.56 667.13 466.97"/>
              <polyline className="cls-1 lines" points="884.13 435.97 991.13 287.56 884.13 123.97"/>
              <line className="cls-1 lines" x1="601.13" y1="565.97" x2="708.98" y2="566.51"/>
              <polygon className="cls-1 lines" points="351.6 264.58 413.53 375.74 313.17 557.08 307.92 566.6 189.56 566.51 186.78 562.34 199.58 539.2 351.6 264.58"/>
              <polyline className="cls-1 lines" points="721.13 565.97 597.44 566.6 592.16 557.08 499.77 390.06 454.48 308.28 452.68 305.02 390.74 193.84 390.95 193.48 395.9 184.53 436.19 111.75 440.93 101.51 464.58 101.51 469.15 111.72 509.46 184.53 667.1 469.36"/>
              <path className="cls-1 lines" d="M664.13,468c40.43,0,95.73-17.23,121.35-48.31V101.8H883.9V450.05l-2.65,4.4c-16.87,27.91-41.07,69.45-67.89,88-39,27-94.61,23.48-142.23,23.48"/>
            </g>
          </g>
        </svg>
        </DrawSVG> :
        <div>
        </div>
      }
      
    </div>;
}
 
export default Logo;
