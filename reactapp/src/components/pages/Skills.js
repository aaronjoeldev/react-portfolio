
import Tagcanvas from '../partials/Tagcanvas';
import DataGerman from '../../assets/german.json';
import DataEnglish from '../../assets/english.json';



function Skills() {
    let data;

    if(process.env.REACT_APP_ENV === "german") {
        data = DataGerman;
      } else {
        data = DataEnglish;
      }

  return <div class="row page skills">
  <div class="col-lg-6 col-md-6 skills__col">
      <h1 class="skills__col__h1 contrast-h1 sec-color">
          {data.skills.headline}
      </h1>
      <p class="skills__col__text sec-color text-m">
      {data.skills.text}
      </p>


  </div>
  <div class="col-lg-6 col-md-6 skills__col">
      <Tagcanvas></Tagcanvas>
         </div>
  </div>;
}
 
export default Skills;


