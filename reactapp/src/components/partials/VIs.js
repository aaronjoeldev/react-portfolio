import React from 'react';
import Graph from "react-graph-vis";



function Vis() {
    const white = '#ededed';
    const red = '#ff073a';
    const graph = {
        nodes: [
            {
                id: 1,
                label: '2013',
                color: {
                    background: red,
                    border: white
                },
                font:{size: 30, color: red, face: 'Pathway Gothic One'},
                group: 1
              },
              {
                id: 2,
                label: '2016',
                color: {
                    background: red,
                    border: white
                },
                font:{size: 30, color: red, face: 'Pathway Gothic One'},
                group: 1
              },
              {
                id: 3,
                label: '2020',
                color: {
                    background: red,
                    border: white
                },
                font:{size: 30, color: red, face: 'Pathway Gothic One'},
                group: 1
              },
              {
                id: 4,
                label: '2021',
                color: {
                    background: red,
                    border: white
                },
                group: 1,
                font:{size: 30, color: red, face: 'Pathway Gothic One'}
              },
              {
                id: 6,
                label: '<b>Abitur</b>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2,
              },
              {
                id: 7,
                label: '<i>Maximilian-Kolbe-Gymnasium</i>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 8,
                label: '<b>Medieninformatik</b>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 9,
                label: '<i>Technische Hochschule Köln</i>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 10,
                label: '<b>Mediengestalter</b>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 11,
                label: '<i>Ausbildung abgeschlossen</i>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 12,
                label: '<b>Frontend-Entwickler</b>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2
              },
              {
                id: 13,
                label: '<i>Angestellt</i>',
                color: white,
                font:{size: 5, face: 'Archivo'},
                group: 2,
              },
        ],
        edges: [
            {from: 1, to: 2},
            {from: 1, to: 6},
            {from: 1, to: 7},
            {from: 2, to: 3},
            {from: 2, to: 8},
            {from: 2, to: 9},
            {from: 3, to: 4},
            {from: 3, to: 10},
            {from: 3, to: 11},
            {from: 4, to: 12},
            {from: 4, to: 13},
        ]
      };
    
      const options = 
      {
        nodes: {
            size: 10,
            shape: 'dot',
            color: red,
        font: {
          size: 12,
          multi: true,
          strokeWidth: 0,
          color: white
        },
        
        scaling: {
          min: 10,
          max: 100,
        },
      },
      physics: {
        stabilization: {
          iterations: 500     // YMMV; higher value takes longer but results in better layout
        },
        solver: 'barnesHut',
		barnesHut: {
			gravitationalConstant: -24300,
			springConstant: 0.1,
            springLength: 30,
			avoidOverlap: 1,
		},
        stabilization: { iterations: 2500 },
      },
        groups: {
            1: {color:{background: red}, font:{size: 30, color: red, face: 'Pathway Gothic One'}, size: 10},
            2: {color:{background: white}, font:{size: 5, face: 'Archivo'},size: 10}
          }, 
        edges: {
            color: {color: white},
            smooth: false,
            width: 3
        }
      };
    

      var moveToOptions = {
        position: {x:300, y:300},    // position to animate to (Numbers)
        scale: 3.0,              // scale to animate to  (Number)
        //offset: {x:300, y:300},      // offset from the center in DOM pixels (Numbers)
        animation: {             // animation object, can also be Boolean
        duration: 1000,                 // animation duration in milliseconds (Number)
        easingFunction: "easeInOutQuad" // Animation easing function, available are:
        }                                   // linear, easeInQuad, easeOutQuad, easeInOutQuad,
    } 



      const events = {
          
        select: 
        ({ pointer: { canvas, nodes } }) => {
            moveToOptions.position.x = canvas.x;
            moveToOptions.position.y = canvas.y;
          }
      };




  return <div className="vis">
     <Graph
      graph={graph}
      options={options}
      events={events}
      scale={100}
      getNetwork={network => {
          network.on('click', function(properties) {
            network.moveTo(moveToOptions);
    });
          
        //  if you want access to vis.js network api you can set the state in a parent component using this property
      }}
    />
  </div>;
}
 
export default Vis;


