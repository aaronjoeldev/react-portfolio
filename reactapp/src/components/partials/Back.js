
import React, {useState, useEffect,} from 'react';
import DrawSVG from 'react-svg-drawing'
import { Link } from 'react-router-dom';



const Back = (props) =>  {
  const [hover, setHover] = useState(false);
  const [hide, setHide] = useState(true);




useEffect(() => {
  setTimeout(function(){
    setHide(false);
  }, 2500)
}, []);


  return <div className={hide ? "d-none" : "back"} onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}>
  <Link to="/">
      {hover ?
        <DrawSVG
      targets = ".arrow"
      easing = "easeInOutExpo"
      strokeWidth = "3" 
      duration = "1000">
        <svg version="1.1" id="Ebene_2"  x="0px" y="0px"
	 viewBox="0 0 100 100">
      <path class="arrow" d="M66.1,97c-1.5-0.5-2.5-1.5-3.6-2.6c-13.3-13.3-26.6-26.6-40-40c-1.9-1.9-2.3-4-1.1-6c0.3-0.4,0.6-0.8,1-1.2
        c13.7-13.7,27.4-27.4,41.1-41c1.4-1.4,3.1-2,5-1.4c1.8,0.6,2.8,1.9,3.2,3.7c0.2,1.4-0.2,2.7-1.1,3.8c-0.3,0.3-0.5,0.6-0.8,0.9
        C57.5,25.4,45.2,37.7,32.9,50c-0.3,0.3-0.6,0.4-0.9,0.6c0,0.1,0,0.3,0,0.4c0.3,0.2,0.6,0.4,0.9,0.6C45.3,63.9,57.6,76.3,70,88.7
        c1.9,1.9,2.3,4.1,1,6.1C70.3,96,69.2,96.5,68,97C67.4,97,66.8,97,66.1,97z"/> 
      </svg>
        </DrawSVG> 
        :
        
        <svg version="1.1" id="Ebene_2"  x="0px" y="0px"
        viewBox="0 0 100 100">
      <path d="M66.1,97c-1.5-0.5-2.5-1.5-3.6-2.6c-13.3-13.3-26.6-26.6-40-40c-1.9-1.9-2.3-4-1.1-6c0.3-0.4,0.6-0.8,1-1.2
        c13.7-13.7,27.4-27.4,41.1-41c1.4-1.4,3.1-2,5-1.4c1.8,0.6,2.8,1.9,3.2,3.7c0.2,1.4-0.2,2.7-1.1,3.8c-0.3,0.3-0.5,0.6-0.8,0.9
        C57.5,25.4,45.2,37.7,32.9,50c-0.3,0.3-0.6,0.4-0.9,0.6c0,0.1,0,0.3,0,0.4c0.3,0.2,0.6,0.4,0.9,0.6C45.3,63.9,57.6,76.3,70,88.7
        c1.9,1.9,2.3,4.1,1,6.1C70.3,96,69.2,96.5,68,97C67.4,97,66.8,97,66.1,97z"/>
      </svg>
   
      }
      </Link>
    </div>;
}
 
export default Back;
