import React from 'react';
import {Carousel} from '3d-react-carousal';
import Ref1 from '../../assets/img/react-memory.webp'
import Ref2 from '../../assets/img/corona-react.webp'
import Ref3 from '../../assets/img/react-recipe.webp'


let slides = [
    <img  src={Ref1} alt="Referenz 1" width="280" height="70"/>,
    <img  src={Ref2} alt="Referenz 2" width="280" height="70" />  ,
    <img  src={Ref3} alt="Referenz 3" width="280" height="70" />  ,
    <img src={Ref2} alt="Referenz 1" width="280" height="70" />   ];


  function Slides() {
  return <div className="carousel"> 
  <Carousel slides={slides} autoplay={true} interval={2000}/>
</div>;
}
 
export default Slides;
