import React from 'react';
import Vis from '../partials/VIs';
import DataGerman from '../../assets/german.json';
import DataEnglish from '../../assets/english.json';


  function Vita() {
    let data;
    if(process.env.REACT_APP_ENV === "german") {
        data = DataGerman;
      } else {
        data = DataEnglish;
      }

  return <div className="row page vita"> 
   <div class="col-lg-4 col-md-4 vita__col">
        <h1 class="vita__col__h1 contrast-h1 sec-color">
            {data.vita.headline}
        </h1>
        <p class="vita__col__text sec-color text-m">
            {data.vita.text}
        </p>

    </div>
    <div class="col-lg-8 col-md-8 vita__col">
        <Vis></Vis>
    </div>
</div>;
}
 
export default Vita;
