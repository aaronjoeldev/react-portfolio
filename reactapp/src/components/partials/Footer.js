import React, {useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBasketballBall, faEnvelope, faEnvelopeOpenText } from "@fortawesome/free-solid-svg-icons";
import LogoHome from './LogoHome';

  function Footer() {

    const [hover, setHover] = useState(false);
      

  React.useEffect(() => {
  }, []);


  return <div className="row footer">
  <div className="col-lg-3 col-md-3 col-12 d-flex align-items-center">
    <LogoHome></LogoHome>
    </div>
    <div className="col-12 col-md-6 col-lg-6">
        
        <div className="footer__socials">
            <a href="https://dribbble.com/AaronJoel" target="_blank" className="footer__socials__link sec-color">
                <FontAwesomeIcon icon={faBasketballBall} />
            </a>
            <a href="mailto:info@aaronjoel.de" className="footer__socials__link sec-color" onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}>
                <FontAwesomeIcon icon={hover ? faEnvelopeOpenText : faEnvelope} />
            </a>
            
        </div>
    </div>
    <div className="col-lg-3 col-md-3 col-12 d-flex align-items-center justify-content-center text-center sec-color">
      <div className="footer__address text-s">
        <p>Aaron Joel Kaemmerer-Karsten</p>
        <p>Rønnevej 119</p>
        <p>3700 Rønne</p>
      </div>
    </div>
  </div>;
}
 
export default Footer;
